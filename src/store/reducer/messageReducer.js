import {CHANGE_VALUE,SEND_MESSAGE_DECODE_SUCCESS, SEND_MESSAGE_ENCODE_SUCCESS} from "../action/messages";

const initialState ={
    password: '',
    encode: '',
    decode: ''

};

const messageReducer =(state = initialState, action) =>{
    switch (action.type) {
        case SEND_MESSAGE_ENCODE_SUCCESS:
            return{
                ...state,
                decode: action.decode,
            };
        case SEND_MESSAGE_DECODE_SUCCESS:
            return{
                ...state,
                encode: action.encode,
            };
        case CHANGE_VALUE:
            return{
                ...state,
                [action.input.target.name] : action.input.target.value
            };
        default:
            return state;
    }
};

export default messageReducer;