import axios from '../../axios-message';

export const SEND_MESSAGE_ENCODE_SUCCESS ='SEND_MESSAGE_ENCODE_SUCCESS';
export const SEND_MESSAGE_DECODE_SUCCESS ='SEND_MESSAGE_DECODE_SUCCESS';
export const CHANGE_VALUE = 'CHANGE_VALUE';


export const sendMessageEncodeSuccess = (decode) =>({type: SEND_MESSAGE_ENCODE_SUCCESS , decode});
export const sendMessageDecodeSuccess =(encode)=>({type: SEND_MESSAGE_DECODE_SUCCESS , encode});
export const changeValue = input =>({type: CHANGE_VALUE , input});



export const sendEncode = () =>{
    return  (dispatch, getState) =>{
        const data = {
            message: getState().message.encode,
            password: getState().message.password
        };
        axios.post('/message/decode', data).then(
            (response)=>dispatch(sendMessageEncodeSuccess(response.data))
        )
    }
};

export const sendDecode = () =>{
    return  (dispatch, getState) =>{
        const data = {
            message: getState().message.decode,
            password: getState().message.password
        };
        axios.post('/message/encode', data).then(
            (response)=>dispatch(sendMessageDecodeSuccess(response.data))
        )
    }
};
