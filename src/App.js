import React, { Component } from 'react';
import './App.css';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {
    changeValue,
    sendDecode,
    sendEncode,
} from "./store/action/messages";
import {connect} from "react-redux";

class App extends Component {


  render() {
    return (
      <div className="App">
          <Form>
              <FormGroup row>
                  <Label for="Encode" sm={2}>Encode</Label>
                  <Col sm={10}>
                      <Input value={this.props.encode} type="textarea" onChange={this.props.changeValue} name="encode" id="Encode" />
                  </Col>
              </FormGroup>
              <FormGroup row>
                  <Label for="Password" sm={2}>Password</Label>
                  <Col sm={7}>
                  <Input type="password" value={this.props.password} onChange={this.props.changeValue} name="password" id="Password" placeholder="Password" />
                  </Col>
                  <Button color="secondary" sm={2} className="buttonDecode" onClick={this.props.sendDecode}> &and;</Button>
                  <Button color="secondary" sm={2} className="buttonEncode" onClick={this.props.sendEncode}>&or;</Button>
              </FormGroup>
              <FormGroup row>
                  <Label for="Decode" sm={2}>Decode</Label>
                  <Col sm={10}>
                      <Input type="textarea" value={this.props.decode} onChange={this.props.changeValue} name="decode" id="Decode"  />
                  </Col>
              </FormGroup>
          </Form>
      </div>
    );
  }
}

const mapStateToProps = state =>({
    encode: state.message.encode,
    decode: state.message.decode,
    password: state.message.password,
});

const mapDispatchToProps = dispatch =>{
    return{
    changeValue: event => dispatch(changeValue(event)),
        sendEncode: ()  => dispatch(sendEncode()),
    sendDecode: ()  => dispatch(sendDecode()),

}};

export default connect(mapStateToProps,mapDispatchToProps)(App);
